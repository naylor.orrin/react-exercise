import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red, blue } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Paper, Table, TableRow, TableCell, TableBody } from '@material-ui/core';
import PropTypes from 'prop-types'
const useStyles = makeStyles(theme => ({
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        borderRadius: 0,
    },
    expandOpen: {
        transform: 'rotate(180deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    avatarRepublican: {
        backgroundColor: red[500],
    },
    avatarDemocrat: {
        backgroundColor: blue[500],
    },
    fullWidth: {
        width: '100%'
    },
    centerText: {
        textAlign: 'center'
    },
}));

function CongressCard({ name, party, district, phone, office, link }) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    function handleExpandClick() {
        setExpanded(!expanded);
    }

    return (
        <Card className={clsx(classes.card)}>
            <CardHeader
                avatar={
                    <Avatar aria-label="Recipe" className={clsx({
                        [classes.avatarRepublican]: party === 'Republican',
                        [classes.avatarDemocrat]: party === 'Democrat'
                    })}>
                        {district}
                    </Avatar>
                }
                title={name}
            />
            <IconButton
                className={clsx(classes.expand, classes.fullWidth)}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="Show more"
            >
                <ExpandMoreIcon className={clsx({
                    [classes.expandOpen]: expanded,
                })} />
            </IconButton>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableBody>
                            <TableRow key={name + 1}>
                                <TableCell component="th" scope="row">Name</TableCell>
                                <TableCell component="th" scope="row">{name}</TableCell>
                            </TableRow>
                            <TableRow key={name + 2}>
                                <TableCell component="th" scope="row">Party</TableCell>
                                <TableCell component="th" scope="row">{party}</TableCell>
                            </TableRow>
                            <TableRow key={name + 3}>
                                <TableCell component="th" scope="row">District</TableCell>
                                <TableCell component="th" scope="row">{district}</TableCell>
                            </TableRow>
                            <TableRow key={name + 4}>
                                <TableCell component="th" scope="row">Phone</TableCell>
                                <TableCell component="th" scope="row">{phone}</TableCell>
                            </TableRow>
                            <TableRow key={name + 5}>
                                <TableCell component="th" scope="row">Office</TableCell>
                                <TableCell component="th" scope="row">{office}</TableCell>
                            </TableRow>
                            <TableRow key={name + 6}>
                                <TableCell component="th" scope="row">Link</TableCell>
                                <TableCell component="th" scope="row"><a href={link} alt="Visit The Reps Site">{link}</a></TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Paper>
            </Collapse>
        </Card>
    );
}
CongressCard.propTypes = {
    name: PropTypes.string.isRequired,
    party: PropTypes.string.isRequired,
    district: PropTypes.string,
    phone: PropTypes.string.isRequired,
    office: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
}
export default CongressCard;