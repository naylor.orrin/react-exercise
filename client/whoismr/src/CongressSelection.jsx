import React from 'react';
import { Card, Container, Grid, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import ArrowLeft from '@material-ui/icons/ArrowLeft'
import ArrowRight from '@material-ui/icons/ArrowRight'
const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(3),
    width: '90%',
  },
  input: {
    display: 'none',
  },
  icon: {
    margin: theme.spacing(1),
    fontSize: 32,
  },
  textRight: {
    textAlign: 'right'
  },
  textLeft: {
    textAlign: 'left'
  }
}));

const CongressSelection = ({ history }) => {
  const classes = useStyles();
  const navigateToRepresentatives = () => {
    history.push('/representativesUI')
  }
  const navigateToSenators = () => {
    history.push('/senatorsUI')
  }
  return (
    <div className="page-container page">
    <Container>
      <Grid container spacing={3}>
        <Grid item sm={1} xs={true} lg={2}></Grid>
        <Grid item sm={10} xs={12} lg={8}>
          <Card>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Button variant="contained" color="primary" className={classes.button} size="large" onClick={navigateToRepresentatives}>
                   <Grid xs={1}><ArrowLeft className={classes.icon} /></Grid><Grid xs={11} className={classes.textRight}>Representatives</Grid>
                </Button>
                <Button variant="contained" color="secondary" className={classes.button} size="large" onClick={navigateToSenators}>
                  <Grid xs={11} className={classes.textLeft}>Senators</Grid><Grid xs={1}><ArrowRight className={classes.icon} /></Grid>
                </Button>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item sm={1} xs={true} lg={2}></Grid>
      </Grid>
    </Container>
    </div>
  );
}
export default CongressSelection;
