import React from 'react'
import CachingContext from './CachingContext'
class CachingContextProvidor extends React.Component {
    constructor(props) {
        super(props);
        const congressCachJSON = window.localStorage.getItem('congress');
        let congressCach = {}
        if (congressCachJSON) {
            try {
                congressCach = JSON.parse(congressCachJSON)
            }
            catch (e) {
                console.error("Congress cach was malformed")
                console.error(e)
            }
        }
        this.congressCachLocal = congressCach;
        this.allStates = [
            'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY',
        ]
        this.getCongressPersons = this.getCongressPersons.bind(this)
    }
    async getCongressPersons(USstate, congressPersonType) {
        const congressState = this.congressCachLocal[USstate]
        if (congressState) {
            const congressPersons = congressState[congressPersonType]
            if (congressPersons) {
                return congressPersons;
            }
        }
        return this.getCongressFromApi(USstate, congressPersonType);
    }
    async getCongressFromApi(USstate, congressPersonType) {
        const congressPersons = await fetch(`/${congressPersonType}/${USstate}`).then(res => res.json())
        if (congressPersons && congressPersons.success) {
            return congressPersons.results
        }
    }
    saveCongressCach(congressCach) {
        window.localStorage.setItem('congress', JSON.stringify(congressCach))
    }
    async componentDidMount() {
        for (let i = 0; i < this.allStates.length; i++) {
            if (!this.congressCachLocal[this.allStates[i]]) {
                this.congressCachLocal[this.allStates[i]] = {}
            }
            if (!this.congressCachLocal[this.allStates[i]]['representatives']) {
                const congressPeople = await this.getCongressFromApi(this.allStates[i], 'representatives')
                this.congressCachLocal[this.allStates[i]]['representatives'] = congressPeople;
                this.saveCongressCach(this.congressCachLocal)
            }
            if (!this.congressCachLocal[this.allStates[i]]['senators']) {
                const congressPeople = await this.getCongressFromApi(this.allStates[i], 'senators')
                this.congressCachLocal[this.allStates[i]]['senators'] = congressPeople;
                this.saveCongressCach(this.congressCachLocal)
            }
        }
    }
    render() {
        const { children } = this.props
        return <CachingContext.Provider value={{ getCongressPersons: this.getCongressPersons }}>
            {children}
        </CachingContext.Provider>
    }
}
export default CachingContextProvidor