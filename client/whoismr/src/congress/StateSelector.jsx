import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Card, Container, Grid, Button } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        width: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function StateSelector({ history, congressPersonType, currentState }) {
    const classes = useStyles();
    let currentStateCalculated = ''
    if(currentState){
        currentStateCalculated = currentState
    }

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    function handleChange(event) {
        history.push(`/${congressPersonType}/${event.target.value}`)
    }

    return (<Container>
        <Grid container spacing={3}>
            <Grid item sm={1} xs={false} lg={2}></Grid>
            <Grid item sm={10} xs={12} lg={8}>
                <Card>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <form className={classes.root} autoComplete="off">
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel ref={inputLabel} htmlFor="outlined-age-simple">
                                        State
        </InputLabel>
                                    <Select
                                        value={currentStateCalculated}
                                        onChange={handleChange}
                                        input={<OutlinedInput labelWidth={labelWidth} name="age" id="outlined-age-simple" />}
                                    >
                                        <MenuItem value={'AL'}>Alabama</MenuItem>
                                        <MenuItem value={'AK'}>Alaska</MenuItem>
                                        <MenuItem value={'AZ'}>Arizona</MenuItem>
                                        <MenuItem value={'AR'}>Arkansas</MenuItem>
                                        <MenuItem value={'CA'}>California</MenuItem>
                                        <MenuItem value={'CO'}>Colorado</MenuItem>
                                        <MenuItem value={'CT'}>Connecticut</MenuItem>
                                        <MenuItem value={'DE'}>Delaware</MenuItem>
                                        <MenuItem value={'FL'}>Florida</MenuItem>
                                        <MenuItem value={'GA'}>Georgia</MenuItem>
                                        <MenuItem value={'HI'}>Hawaii</MenuItem>
                                        <MenuItem value={'ID'}>Idaho</MenuItem>
                                        <MenuItem value={'IL'}>Illinois</MenuItem>
                                        <MenuItem value={'IN'}>Indiana</MenuItem>
                                        <MenuItem value={'IA'}>Iowa</MenuItem>
                                        <MenuItem value={'KS'}>Kansas</MenuItem>
                                        <MenuItem value={'KY'}>Kentucky</MenuItem>
                                        <MenuItem value={'LA'}>Louisiana</MenuItem>
                                        <MenuItem value={'ME'}>Maine</MenuItem>
                                        <MenuItem value={'MD'}>Maryland</MenuItem>
                                        <MenuItem value={'MA'}>Massachusetts</MenuItem>
                                        <MenuItem value={'MI'}>Michigan</MenuItem>
                                        <MenuItem value={'MN'}>Minnesota</MenuItem>
                                        <MenuItem value={'MS'}>Mississippi</MenuItem>
                                        <MenuItem value={'MO'}>Missouri</MenuItem>
                                        <MenuItem value={'MT'}>Montana</MenuItem>
                                        <MenuItem value={'NE'}>Nebraska</MenuItem>
                                        <MenuItem value={'NV'}>Nevada</MenuItem>
                                        <MenuItem value={'NH'}>New Hampshire</MenuItem>
                                        <MenuItem value={'NJ'}>New Jersey</MenuItem>
                                        <MenuItem value={'NM'}>New Mexico</MenuItem>
                                        <MenuItem value={'NY'}>New York</MenuItem>
                                        <MenuItem value={'NC'}>North Carolina</MenuItem>
                                        <MenuItem value={'ND'}>North Dakota</MenuItem>
                                        <MenuItem value={'OH'}>Ohio</MenuItem>
                                        <MenuItem value={'OK'}>Oklahoma</MenuItem>
                                        <MenuItem value={'OR'}>Oregon</MenuItem>
                                        <MenuItem value={'PA'}>Pennsylvania</MenuItem>
                                        <MenuItem value={'RI'}>Rhode Island</MenuItem>
                                        <MenuItem value={'SC'}>South Carolina</MenuItem>
                                        <MenuItem value={'SD'}>South Dakota</MenuItem>
                                        <MenuItem value={'TN'}>Tennessee</MenuItem>
                                        <MenuItem value={'TX'}>Texas</MenuItem>
                                        <MenuItem value={'UT'}>Utah</MenuItem>
                                        <MenuItem value={'VT'}>Vermont</MenuItem>
                                        <MenuItem value={'VA'}>Virginia</MenuItem>
                                        <MenuItem value={'WA'}>Washington</MenuItem>
                                        <MenuItem value={'WV'}>West Virginia</MenuItem>
                                        <MenuItem value={'WI'}>Wisconsin</MenuItem>
                                        <MenuItem value={'WY'}>Wyoming</MenuItem>
                                    </Select>
                                </FormControl>
                            </form>
                        </Grid>
                    </Grid>
                </Card>
                <Grid item sm={1} xs={false} lg={2}></Grid>
            </Grid>
        </Grid>
    </Container>
    );
}