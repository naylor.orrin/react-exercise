import React from 'react'
import CongressCard from './CongressCard'
import { Container, Grid } from '@material-ui/core';
import CachingContext from '../cachingContext/CachingContext'

class CongressPersonList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            congressPeople: [],
            outOfSite: true,
            addTransistion: false,
            bringItBack: false,
            isLoading: true,
        }
        this.removeOuttaSite = this.removeOuttaSite.bind(this)
        this.bringItBack = this.bringItBack.bind(this)
        this.endAnimation = this.endAnimation.bind(this)
    }
    async fetchNewCongressInfo() {
        const { getCongressPersons } = this.props
        if (!this.props.match || !this.props.match.params) {
            return;
        }
        const { match: { params: { currentState, congressPersonType } } } = this.props
        if (currentState && congressPersonType) {
            const currnetStateClean = currentState.replace('UI', '')
            const congressPersonTypeClean = congressPersonType.replace('UI', '')
            const congressPeople = await getCongressPersons(currnetStateClean, congressPersonTypeClean)
            if (congressPeople && congressPeople.length > 0) {
                this.setState({
                    congressPeople: congressPeople.sort((a, b) => (parseInt(a.district) - parseInt(b.district))),
                    isLoading: false,
                })
            }
        }

    }
    componentDidMount() {
        this.fetchNewCongressInfo()
    }
    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({
                outOfSite: true,
                addTransistion: false,
                bringItBack: false,
                isLoading: true,
            })
            this.fetchNewCongressInfo()
        }
    }
    removeOuttaSite() {
        this.setState({
            outOfSite: false,
            addTransistion: true
        })
    }
    bringItBack() {
        this.setState({
            addTransistion: false,
            bringItBack: true,
        })
    }
    endAnimation() {
        this.setState({
            bringItBack: false,
        })
    }
    render() {
        const { congressPeople, outOfSite, addTransistion, bringItBack, isLoading } = this.state
        if (!congressPeople || congressPeople.length === 0 || isLoading) {
            return null
        }
        let containerClassNames = ''
        if (outOfSite) {
            containerClassNames = 'outtaSite'
            setTimeout(this.removeOuttaSite, 10)
        }
        if (addTransistion) {
            containerClassNames = 'outtaSite addTransistion'
            setTimeout(this.bringItBack, 10)
        }
        if (bringItBack) {
            containerClassNames = 'addTransistion bringItBack'
            setTimeout(this.endAnimation, 200)
        }
        if (outOfSite || addTransistion || bringItBack) {
            document.body.classList.add('hideOverflow');
        }
        else {
            document.body.classList.remove('hideOverflow');
        }
        return (
            <div className={containerClassNames}>
                {congressPeople.map(({ name, party, district, phone, office, link }) => (
                    <Container key={name}>
                        <Grid container spacing={3}>
                            <Grid item sm={1} xs={false} lg={2}></Grid>
                            <Grid item sm={10} xs={12} lg={8}><CongressCard name={name} party={party} district={district} phone={phone} office={office} link={link} /></Grid>
                        </Grid>
                    </Container>
                ))}
            </div>
        )
    }
}
export default ({ ...props }) => (
    <CachingContext.Consumer>
        {(
            { getCongressPersons },
        ) => <CongressPersonList {...props} getCongressPersons={getCongressPersons} />
        }
    </CachingContext.Consumer>)