import React from 'react';
import { Switch, Route } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import CongressSelection from './CongressSelection'
import CongressPage from './congress/CongressPage'
import './App.css'

let lastKey = ''
class App extends React.Component {
  render() {
    const { location } = this.props;
    const currentKey = location.pathname.split("/")[1] || "/"
    const timeout = { enter: 800, exit: 400 };
    let transitionDirection = 'right'

    if (currentKey === 'representativesUI') {
      transitionDirection = 'left'
    }
    else if(currentKey === '/'){
      if(lastKey === 'senatorsUI'){
        transitionDirection = 'left'
      }
    }
    if(currentKey !== '/'){
      lastKey = currentKey
    }
    return (
      <TransitionGroup component="div">
        <CSSTransition
          key={currentKey}
          timeout={timeout}
          classNames="pageSlider"
          mountOnEnter={false}
          unmountOnExit={true}
        >
          <div className={transitionDirection}>
            <Switch>
              <Route exact path='/' component={CongressSelection}></Route>
              <Route location={location} path='/:congressPersonType(representativesUI|senatorsUI)/:currentState?' component={CongressPage}></Route>
            </Switch>
          </div>
        </CSSTransition>
      </TransitionGroup>
    )
  }
}

export default App;
