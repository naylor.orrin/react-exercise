import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
import { Tooltip, Fab } from '@material-ui/core';
import ArrowRight from '@material-ui/icons/ArrowRight'
import ArrowLeft from '@material-ui/icons/ArrowLeft'
import StateSelector from './StateSelector'
import CongressPersonList from './CongressPersonList'
const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(2),
    },
    absoluteRight: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(3),
    },
    absoluteLeft: {
        position: 'fixed',
        bottom: theme.spacing(2),
        left: theme.spacing(3),
    },
}));
const CongressPage = ({ history, match: { params: { congressPersonType, currentState } }, location }) => {
    const classes = useStyles();
    const goToMainPage = () => {
        history.push('/')
    }
    return (
        <>
            <div className="page-container page">
                <StateSelector history={history} congressPersonType={congressPersonType} currentState={currentState} />
                <Switch location={location}>
                    <Route path='/:congressPersonType(representativesUI|senatorsUI)/:currentState' component={CongressPersonList}></Route>
                </Switch>
            </div>
            {congressPersonType === 'representativesUI' &&
                <Tooltip title="Go Back" aria-label="Go Back">
                    <Fab color="primary" className={classes.absoluteRight} onClick={goToMainPage}>
                        <ArrowRight />
                    </Fab>
                </Tooltip>
            }
            {congressPersonType === 'senatorsUI' &&
                <Tooltip title="Go Back" aria-label="Go Back">
                    <Fab color="secondary" className={classes.absoluteLeft} onClick={goToMainPage}>
                        <ArrowLeft />
                    </Fab>
                </Tooltip>
            }
        </>
    )
}
export default CongressPage